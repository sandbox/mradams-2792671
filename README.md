Quizily
=======


What Is This?
-------------

This module allows admins to create simple multiple choice quizzes. Quiz
Questions are stored as configuration entities while Quizzes are stored
as content entities.

TODO (There's Lots!)
--------------------

1. Create form on the quiz take tab (Multi Step)
1. Get questions to work with inline entity forms
1. Create System Admin Menu Block
1. Create Quiz Settings
1. Create Psuedo Field for the "Take" link
