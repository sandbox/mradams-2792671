<?php

/**
 * @file
 * Contains quizily_quiz.page.inc.
 *
 * Page callback for quizily Quiz entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Quiz templates.
 *
 * Default template: quizily_quiz.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_quizily_quiz(array &$variables) {
  // Fetch QuizilyQuiz Entity Object.
  $quizily_quiz = $variables['elements']['#quizily_quiz'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
