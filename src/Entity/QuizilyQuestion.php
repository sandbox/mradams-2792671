<?php

namespace Drupal\quizily\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Question entity.
 *
 * @ConfigEntityType(
 *   id = "quizily_question",
 *   label = @Translation("Question"),
 *   handlers = {
 *     "list_builder" = "Drupal\quizily\QuizilyQuestionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\quizily\Form\QuizilyQuestionForm",
 *       "edit" = "Drupal\quizily\Form\QuizilyQuestionForm",
 *       "delete" = "Drupal\quizily\Form\QuizilyQuestionDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\quizily\QuizilyQuestionHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "quizily_question",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/quizily_question/{quizily_question}",
 *     "add-form" = "/admin/structure/quizily_question/add",
 *     "edit-form" = "/admin/structure/quizily_question/{quizily_question}/edit",
 *     "delete-form" = "/admin/structure/quizily_question/{quizily_question}/delete",
 *     "collection" = "/admin/structure/quizily_question"
 *   }
 * )
 */
class QuizilyQuestion extends ConfigEntityBase implements QuizilyQuestionInterface {

  /**
   * The Question ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Question label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Question question.
   *
   * @var string
   */
  protected $question;

  /**
   * The Question options.
   *
   * @var array
   */
  protected $options;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->question;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    return $this->options;
  }

}
