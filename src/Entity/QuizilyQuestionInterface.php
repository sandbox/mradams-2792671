<?php

namespace Drupal\quizily\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Question entities.
 */
interface QuizilyQuestionInterface extends ConfigEntityInterface {

  /**
   * Gets the question value.
   *
   * @return string
   *   Question string
   */
  public function getQuestion();

  /**
   * Gets the question options.
   *
   * @return array
   *   Array of possible answers.
   */
  public function getOptions();

}
