<?php

namespace Drupal\quizily\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining quizily Quiz entities.
 *
 * @ingroup quizily
 */
interface QuizilyQuizInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the quizily Quiz name.
   *
   * @return string
   *   Name of the quizily Quiz.
   */
  public function getName();

  /**
   * Sets the quizily Quiz name.
   *
   * @param string $name
   *   The quizily Quiz name.
   *
   * @return \Drupal\quizily\Entity\QuizilyQuizInterface
   *   The called quizily Quiz entity.
   */
  public function setName($name);

  /**
   * Gets the quizily Quiz creation timestamp.
   *
   * @return int
   *   Creation timestamp of the quizily Quiz.
   */
  public function getCreatedTime();

  /**
   * Sets the quizily Quiz creation timestamp.
   *
   * @param int $timestamp
   *   The quizily Quiz creation timestamp.
   *
   * @return \Drupal\quizily\Entity\QuizilyQuizInterface
   *   The called quizily Quiz entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the quizily Quiz published status indicator.
   *
   * Unpublished quizily Quiz are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the quizily Quiz is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a quizily Quiz.
   *
   * @param bool $published
   *   TRUE to set this quizily Quiz to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\quizily\Entity\QuizilyQuizInterface
   *   The called quizily Quiz entity.
   */
  public function setPublished($published);

  /**
   * Returns Questions that are attached to the Quiz.
   *
   * @return array
   *   Array of Questions
   */
  public function getQuestions();

}
