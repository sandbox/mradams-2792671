<?php

namespace Drupal\quizily\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for quizily Quiz entities.
 */
class QuizilyQuizViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['quizily_quiz']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('quizily Quiz'),
      'help' => $this->t('The quizily Quiz ID.'),
    );

    return $data;
  }

}
