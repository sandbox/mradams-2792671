<?php

namespace Drupal\quizily\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class QuizilyQuestionForm.
 *
 * @package Drupal\quizily\Form
 */
class QuizilyQuestionForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /* @var $quizily_question QuizilyQuestion */
    $quizily_question = $this->entity;

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $quizily_question->label(),
      '#description' => $this->t("Label for the Question."),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $quizily_question->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\quizily\Entity\QuizilyQuestion::load',
      ),
      '#disabled' => !$quizily_question->isNew(),
    );

    $form['question'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Question'),
      '#maxlength' => 255,
      '#default_value' => $quizily_question->getQuestion(),
      '#description' => $this->t("Label for the Question."),
      '#required' => TRUE,
    );

    // TODO: Replace this with a drag and drop interface.
    // TODO: Validate so that only one answer can be correct.
    $i = 0;
    $name_field = $form_state->get('num_options');
    $form['#tree'] = TRUE;
    $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Answers'),
      '#prefix' => '<div id="options-wrapper">',
      '#suffix' => '</div>',
    );

    $options = $quizily_question->getOptions();
    unset($options['actions']);
    // If there is nothing in the form_state first checkout the question or else
    // default to one.
    if (empty($name_field)) {
      if (!empty($options)) {
        $name_field = count($options);
      }
      else {
        $name_field = 1;
      }
      $form_state->set('num_options', $name_field);
    }

    for ($i = 0; $i < $name_field; $i++) {
      $form['options'][$i] = array(
        '#type' => 'container',
      );
      $form['options'][$i]['option'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Multiple Choice Option'),
        '#default_value' => isset($options[$i]['option']) ? $options[$i]['option'] : '',
        '#required' => TRUE,
      ];
      $form['options'][$i]['correct'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Correct Answer'),
        '#default_value' => isset($options[$i]['correct']) ? $options[$i]['correct'] : '',
      ];
    }
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['options']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => t('Add one more'),
      '#submit' => array('::addOne'),
      '#limit_validation_errors' => array(),
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'options-wrapper',
      ],
    ];
    if ($name_field > 1) {
      $form['options']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => t('Remove one'),
        '#submit' => array('::removeCallback'),
        '#limit_validation_errors' => array(),
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'options-wrapper',
        ],
      ];
    }
    $form_state->setCached(FALSE);

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_options');
    return $form['options'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_options');
    $add_button = $name_field + 1;
    $form_state->set('num_options', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_options');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_options', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /* @var $quizily_question QuizilyQuestion */
    $quizily_question = $this->entity;
    $status = $quizily_question->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Question.', [
          '%label' => $quizily_question->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Question.', [
          '%label' => $quizily_question->label(),
        ]));
    }
    $form_state->setRedirectUrl($quizily_question->urlInfo('collection'));
  }

}
