<?php

namespace Drupal\quizily\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting quizily Quiz entities.
 *
 * TODO: We can probably remove this.
 *
 * @ingroup quizily
 */
class QuizilyQuizDeleteForm extends ContentEntityDeleteForm {


}
