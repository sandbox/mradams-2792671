<?php

namespace Drupal\quizily\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for quizily Quiz edit forms.
 *
 * @ingroup quizily
 */
class QuizilyQuizForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\quizily\Entity\QuizilyQuiz */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label quizily Quiz.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label quizily Quiz.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.quizily_quiz.canonical', ['quizily_quiz' => $entity->id()]);
  }

}
