<?php

namespace Drupal\quizily\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class QuizilyQuizSettingsForm.
 *
 * TODO: Redirect page.
 * TODO: Completion message.
 * TODO: Scoring language.
 *
 * @package Drupal\quizily\Form
 *
 * @ingroup quizily
 */
class QuizilyQuizSettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'QuizilyQuiz_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * Defines the settings form for quizily Quiz entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['QuizilyQuiz_settings']['#markup'] = 'Settings form for quizily Quiz entities. Manage field settings here.';
    return $form;
  }

}
