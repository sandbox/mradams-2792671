<?php

namespace Drupal\quizily\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\quizily\Entity\QuizilyQuestionInterface;
use Drupal\quizily\Entity\QuizilyQuizInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Form controller for taking Quizily Quizzes.
 *
 * @ingroup quizily
 */
class QuizilyTakeQuizForm extends FormBase {

  /**
   * Getter method for Form ID.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'quizily_take_quiz_form';
  }

  /**
   * Build the Take Quiz Form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param FormStateInterface $form_state
   *   Object containing current form state.
   * @param QuizilyQuizInterface $quizily_quiz
   *   Quiz object.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, QuizilyQuizInterface $quizily_quiz = NULL) {
    if (empty($quizily_quiz)) {
      throw new NotFoundHttpException();
    }

    // Store quiz in the form_state so that it can be used to score in the
    // submit function.
    $form_state->addBuildInfo('quiz', $quizily_quiz);

    // Load the questions associated with the quiz and build the form based on
    // the options.
    // TODO: Make this a multi step form.
    if ($questions = $quizily_quiz->getQuestions()) {
      /* @var $question QuizilyQuestionInterface */
      foreach ($questions as $question) {
        $label = $question->getOriginalId();
        $title = $question->getQuestion();

        // Build options array.
        $raw_options = $question->getOptions();
        $options = array();
        foreach ($raw_options as $option) {
          if (isset($option['option'])) {
            $options[$option['option']] = $option['option'];
          }
        }

        // Build form element based on options.
        if ($label && $title && $options) {
          $form[$label] = array(
            '#type' => 'radios',
            '#title' => $title,
            '#options' => $options,
            '#required' => TRUE,
          );
        }
      }

      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Submit Quiz'),
      );
    }
    else {
      $form['error'] = array(
        '#type' => 'item',
        '#markup' => $this->t('Please add a question before attempting to take this quiz'),
      );
    }

    return $form;
  }

  /**
   * Grade the completed Quiz.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $build_info = $form_state->getBuildInfo();
    if (isset($build_info['quiz'])) {
      /* @var $quiz QuizilyQuizInterface */
      $quiz = $build_info['quiz'];
      $questions = $quiz->getQuestions();
      $points = 0;
      $total = count($questions);
      $answers = $form_state->getValues();

      // Grade questions.
      /* @var $question QuizilyQuestionInterface */
      foreach ($questions as $question) {
        $label = $question->getOriginalId();
        $answer = $answers[$label];

        // Loop through options and get the correct answer.
        $correct_answer = NULL;
        $options = $question->getOptions();
        foreach ($options as $option) {
          if (isset($option['correct']) && $option['correct'] === TRUE) {
            $correct_answer = $option['option'];
          }
        }

        // If answer matches correct answer then assign a point.
        if (!empty($correct_answer) && $correct_answer === $answer) {
          $points++;
        }
      }

      drupal_set_message(t('Thank you for taking the quiz. You scored @points out of @total', array('@points' => $points, '@total' => $total)));
    }
    else {
      drupal_set_message(t('There has been an error with the quiz. Please check the configuration'), 'error');
    }
  }

}
