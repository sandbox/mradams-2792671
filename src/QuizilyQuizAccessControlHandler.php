<?php

namespace Drupal\quizily;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the quizily Quiz entity.
 *
 * @see \Drupal\quizily\Entity\QuizilyQuiz.
 */
class QuizilyQuizAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\quizily\Entity\QuizilyQuizInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished quizily quiz entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published quizily quiz entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit quizily quiz entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete quizily quiz entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add quizily quiz entities');
  }

}
