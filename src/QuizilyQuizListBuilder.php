<?php

namespace Drupal\quizily;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of quizily Quiz entities.
 *
 * @ingroup quizily
 */
class QuizilyQuizListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\quizily\Entity\QuizilyQuiz */
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.quizily_quiz.edit_form', array(
          'quizily_quiz' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
