<?php

namespace Drupal\quizily;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for quizily_quiz.
 */
class QuizilyQuizTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
